package uz.devops.telegrambot;

import lombok.*;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.configs.TelegramBotConfig;
import uz.devops.telegrambot.di.DefaultServiceProvider;
import uz.devops.telegrambot.handlers.callback.TelegramBotInlineQueryHandler;
import uz.devops.telegrambot.handlers.inline.TelegramBotCallbackQueryHandler;
import uz.devops.telegrambot.handlers.message.TelegramBotMessageHandler;
import uz.devops.telegrambot.helpers.TelegramBotServiceRegistration;
import uz.devops.telegrambot.managers.session.TelegramBotSessionManager;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot
 * @since 1/17/2024 6:55 PM
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TelegramBotManager extends TelegramLongPollingBot {

    /**
     *
     */
    public static final String VERSION = "1.0.0.0";

    /**
     *
     */
    @Getter
    @Setter
    TelegramBotConfig config;

    /**
     *
     */
    @Getter
    private DefaultServiceProvider serviceProvider = new DefaultServiceProvider();

    /**
     *
     */
    private TelegramBotsApi telegramBotsApi;

    /**
     * @return
     */
    public static TelegramBotManagerBuilder createBuilder() {
        return new TelegramBotManagerBuilder();
    }

    /**
     * @param token
     * @return
     * @throws TelegramApiException
     */
    public static TelegramBotManager start(String token) throws TelegramApiException {
        var config = new TelegramBotConfig();
        config.setToken(token);

        var manager = new TelegramBotManager();
        manager.init(config);

        return manager;
    }

    /**
     * @param config
     * @return
     * @throws TelegramApiException
     */
    public static TelegramBotManager start(TelegramBotConfig config) throws TelegramApiException {
        var manager = new TelegramBotManager();
        manager.init(config);

        return manager;
    }

    /**
     * @param config
     */
    public void init(TelegramBotConfig config) throws TelegramApiException {
        this.config = config;

        TelegramBotServiceRegistration.RegisterServices(serviceProvider, this);
        config.getBindedInterfaces().forEach((k, v) -> serviceProvider.bindInterface(k, v));
        serviceProvider.build();

        var commandFactory = serviceProvider.getInstance(TelegramBotCommandFactory.class);
        commandFactory.init();

        telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(this);
    }

    /**
     * @param update
     */
    @Override
    public void onUpdateReceived(Update update) {
        var sessionManager = serviceProvider.getInstance(TelegramBotSessionManager.class);
        sessionManager.startSession(update);

        if (update.hasMessage()) {
            serviceProvider.getInstance(TelegramBotMessageHandler.class).handle(update);
            sessionManager.endSession(update);
            return;
        }

        if (update.hasCallbackQuery()) {
            serviceProvider.getInstance(TelegramBotCallbackQueryHandler.class).handle(update);
            sessionManager.endSession(update);
            return;
        }

        if (update.hasInlineQuery()) {
            serviceProvider.getInstance(TelegramBotInlineQueryHandler.class).handle(update);
            sessionManager.endSession(update);
        }
    }

    /**
     * @return
     */
    @Override
    public String getBotUsername() {
        return "DesignPatternsUsedTelegramBot";
    }

    /**
     * @return
     */
    @Override
    public String getBotToken() {
        return this.config.getToken();
    }
}
