package uz.devops.telegrambot.commands;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.di.ServiceProvider;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands
 * @since 1/19/2024 6:50 PM
 */
public interface TelegramBotCommand {

    /**
     * @param serviceProvider
     */
    void initialize(ServiceProvider serviceProvider);

    /**
     *
     */
    void execute();
}
