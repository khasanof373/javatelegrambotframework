package uz.devops.telegrambot.commands.bases;

import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.devops.telegrambot.commands.factories.annotations.TelegramBotCommandAnnotation;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;
import uz.devops.telegrambot.di.ServiceProvider;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.bases
 * @since 1/31/2024 8:49 PM
 */
@TelegramBotCommandAnnotation(commandType = TelegramBotCommandType.CALLBACK)
public abstract class TelegramBotCallbackCommand extends TelegramBotBaseCommand {

    /**
     *
     */
    protected CallbackQuery callbackQuery;

    /**
     *
     */
    protected String commandData;

    /**
     *
     * @param serviceProvider
     */
    @Override
    public void initialize(ServiceProvider serviceProvider) {
        super.initialize(serviceProvider);
        callbackQuery = update.getCallbackQuery();
        commandData = getCommandData();
    }

    /**
     *
     * @return
     */
    protected String getCommandData(){
        var data = callbackQuery.getData();
        if (data == null || data.isEmpty())
            return "";

        if (!data.contains(",")) {
            return "";
        }

        var idSplitterIndex = data.indexOf(",") + 1;
        return data.substring(idSplitterIndex);
    }

    /**
     *
     * @param message
     */
    protected void sendAnswer(String message) {
        var answerQuery = new AnswerCallbackQuery();
        answerQuery.setCallbackQueryId(callbackQuery.getId());
        answerQuery.setShowAlert(true);
        answerQuery.setText(message);

        sendMessage(answerQuery);
    }

    /**
     * @param message
     */
    protected void sendMessage(AnswerCallbackQuery message) {
        try {
            botManager.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
