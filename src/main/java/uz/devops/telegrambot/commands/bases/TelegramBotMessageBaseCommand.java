package uz.devops.telegrambot.commands.bases;

import org.telegram.telegrambots.meta.api.objects.Message;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.helpers.keyboards.inline.InlineKeyboardMarkupBuilder;
import uz.devops.telegrambot.helpers.keyboards.reply.ReplyKeyboardMarkupBuilder;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands
 * @since 1/19/2024 7:51 PM
 */
public abstract class TelegramBotMessageBaseCommand extends TelegramBotBaseCommand {

    /**
     *
     */
    protected Message message;

    /**
     *
     */
    protected Long chatId;

    /**
     *
     */
    protected String messageText;

    /**
     * @param serviceProvider
     */
    @Override
    public void initialize(ServiceProvider serviceProvider) {
        super.initialize(serviceProvider);

        message = update.getMessage();
        messageText = message.getText();
        chatId = update.getMessage().getChatId();
    }

    /**
     *
     * @return
     */
    public InlineKeyboardMarkupBuilder createInlineButtonsBuilder() {
        var commandFactory = serviceProvider.getInstance(TelegramBotCommandFactory.class);
        return new InlineKeyboardMarkupBuilder(commandFactory);
    }

    /**
     *
     * @return
     */
    public ReplyKeyboardMarkupBuilder createReplyButtonsBuilder() {
        return new ReplyKeyboardMarkupBuilder();
    }
}
