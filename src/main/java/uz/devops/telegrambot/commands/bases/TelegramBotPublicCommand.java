package uz.devops.telegrambot.commands.bases;

import uz.devops.telegrambot.commands.factories.annotations.TelegramBotCommandAnnotation;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands
 * @since 1/19/2024 7:52 PM
 */
@TelegramBotCommandAnnotation(commandType = TelegramBotCommandType.PUBLIC)
public abstract class TelegramBotPublicCommand extends TelegramBotMessageBaseCommand {

}
