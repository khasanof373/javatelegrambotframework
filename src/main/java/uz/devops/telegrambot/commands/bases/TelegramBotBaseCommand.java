package uz.devops.telegrambot.commands.bases;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.devops.telegrambot.TelegramBotManager;
import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.database.models.TelegramBotUserInfo;
import uz.devops.telegrambot.database.services.commands.TelegramBotCommandEntityService;
import uz.devops.telegrambot.database.services.users.TelegramBotUserService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.helpers.TelegramBotUpdateHelper;
import uz.devops.telegrambot.helpers.keyboards.inline.InlineKeyboardMarkupBuilder;
import uz.devops.telegrambot.helpers.keyboards.reply.ReplyKeyboardMarkupBuilder;
import uz.devops.telegrambot.managers.caller.TelegramBotCommandCallManager;
import uz.devops.telegrambot.managers.update.TelegramBotUpdateManager;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands
 * @since 1/19/2024 7:51 PM
 */
public abstract class TelegramBotBaseCommand implements TelegramBotCommand {

    /**
     *
     */
    protected ServiceProvider serviceProvider;

    /**
     *
     */
    protected Update update;

    /**
     *
     */
    protected TelegramBotUserService userService;

    /**
     *
     */
    protected TelegramBotUserInfo userInfo;

    /**
     *
     */
    protected TelegramBotManager botManager;

    /**
     * @param serviceProvider
     */
    @Override
    public void initialize(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
        var updateManager = serviceProvider.getInstance(TelegramBotUpdateManager.class);

        userService = serviceProvider.getInstance(TelegramBotUserService.class);
        update = updateManager.getCurrentUpdate();
        userInfo = userService.getCurrentUser();
        botManager = serviceProvider.getInstance(TelegramBotManager.class);
    }

    /**
     *
     * @param text
     * @param buttonBuilder
     */
    protected void sendMessage(String text, InlineKeyboardMarkupBuilder buttonBuilder) {
        var message = new SendMessage();
        message.setChatId(TelegramBotUpdateHelper.getUpdateChatId(update));
        message.setText(text);
        message.setReplyMarkup(buttonBuilder.build());

        sendMessage(message);
    }

    /**
     *
     * @param text
     * @param buttonBuilder
     */
    protected void sendMessage(String text, ReplyKeyboardMarkupBuilder buttonBuilder) {
        var message = new SendMessage();
        message.setChatId(TelegramBotUpdateHelper.getUpdateChatId(update));
        message.setText(text);
        message.setReplyMarkup(buttonBuilder.build());

        sendMessage(message);
    }

    /**
     * @param message
     */
    protected void sendMessage(SendMessage message) {
        try {
            botManager.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    protected  long  getCommandId(Class<? extends TelegramBotCommand> command) {
        var commandService = serviceProvider.getInstance(TelegramBotCommandEntityService.class);
        return commandService.getCommandId(command);
    }

    /**
     * @param text
     */
    protected void sendMessage(String text) {
        var message = new SendMessage();
        message.setText(text);
        message.setChatId(update.getMessage().getChatId().toString());

        try {
            botManager.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    protected void callCommand(Class<? extends TelegramBotCommand> commandClass) {
        var factory = serviceProvider.getInstance(TelegramBotCommandFactory.class);
        var commandInfo = factory.getCommandInfo(commandClass);
        var command = factory.getCommand(commandInfo);

        var callManager = serviceProvider.getInstance(TelegramBotCommandCallManager.class);
        callManager.call(command);
    }
}
