package uz.devops.telegrambot.commands.bases;

import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import uz.devops.telegrambot.commands.factories.annotations.TelegramBotCommandAnnotation;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;
import uz.devops.telegrambot.di.ServiceProvider;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.bases
 * @since 1/31/2024 8:49 PM
 */
@TelegramBotCommandAnnotation(commandType = TelegramBotCommandType.INLINE)
public abstract class TelegramBotInlineCommand extends TelegramBotBaseCommand {

    /**
     *
     */
    protected InlineQuery inlineQuery;

    /**
     *
     * @param serviceProvider
     */
    @Override
    public void initialize(ServiceProvider serviceProvider) {
        super.initialize(serviceProvider);
        inlineQuery = update.getInlineQuery();
    }
}
