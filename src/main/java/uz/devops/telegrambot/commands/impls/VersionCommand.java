package uz.devops.telegrambot.commands.impls;

import uz.devops.telegrambot.TelegramBotManager;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;
import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.impls
 * @since 1/22/2024 6:32 PM
 */
@CommandNameAnnotation(name = "/version")
public class VersionCommand extends TelegramBotPublicCommand {

    /**
     *
     */
    @Override
    public void execute() {
        sendMessage("Framework version: " + TelegramBotManager.VERSION);
    }
}
