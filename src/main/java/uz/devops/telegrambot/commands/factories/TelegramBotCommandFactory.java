package uz.devops.telegrambot.commands.factories;

import jakarta.activation.CommandInfo;
import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandInfo;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories
 * @since 1/19/2024 6:53 PM
 */
public interface TelegramBotCommandFactory {

    /**
     *
     * @param command
     * @return
     */
    TelegramBotCommandInfo getCommandInfo(Class<? extends TelegramBotCommand> command);

    /**
     *
     * @param lang
     * @param name
     * @return
     */
    TelegramBotCommandInfo getCommandInfo(String lang, String name);

    /**
     *
     * @param name
     * @return
     */
    TelegramBotCommandInfo getCommandInfo(String name);

    /**
     *
     * @param name
     * @return
     */
    TelegramBotCommandInfo getCommandInfoByClassName(String name);

    /**
     *
     * @param type
     * @param lang
     * @param name
     * @return
     */
    TelegramBotCommandInfo getCommandInfo(TelegramBotCommandType type, String lang, String name);

    /**
     *
     * @param name
     * @return
     */
    TelegramBotCommand getBackCommand(String lang, String name);

    /**
     *
     * @param name
     * @return
     */
    TelegramBotCommand getPublicCommand(String lang, String name);

    /**
     *
     * @param commandInfo
     * @return
     */
    TelegramBotCommand getCommand(TelegramBotCommandInfo commandInfo);

    /**
     *
     * @param id
     * @return
     */
    TelegramBotCommand getCallbackCommandById(long id);

    /**
     *
     */
    void init();
}
