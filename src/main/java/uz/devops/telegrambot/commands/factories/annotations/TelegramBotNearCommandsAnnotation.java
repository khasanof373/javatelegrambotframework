package uz.devops.telegrambot.commands.factories.annotations;

import uz.devops.telegrambot.commands.TelegramBotCommand;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories
 * @since 1/19/2024 6:59 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TelegramBotNearCommandsAnnotation {

    /**
     *
     * @return
     */
    TelegramBotNearCommandAnnotation[] value();
}
