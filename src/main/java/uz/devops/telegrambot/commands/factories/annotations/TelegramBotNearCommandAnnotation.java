package uz.devops.telegrambot.commands.factories.annotations;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;

import java.lang.annotation.*;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories
 * @since 1/19/2024 6:59 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(value = TelegramBotNearCommandsAnnotation.class)
public @interface TelegramBotNearCommandAnnotation {

    /**
     *
     * @return
     */
    Class<? extends TelegramBotCommand> commandType();
}
