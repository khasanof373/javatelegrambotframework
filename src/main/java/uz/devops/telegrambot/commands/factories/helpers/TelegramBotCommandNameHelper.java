package uz.devops.telegrambot.commands.factories.helpers;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandName;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.helpers
 * @since 1/22/2024 7:36 PM
 */
public class TelegramBotCommandNameHelper {

    /**
     * @param commandClass
     * @return
     */
    public static TelegramBotCommandName getName(Class<? extends TelegramBotCommand> commandClass) {
        var commandName = new TelegramBotCommandName();
        var nameAnnotation = TelegramBotCommandHelpers.getAnnotationFromType(commandClass, CommandNameAnnotation.class);
        if (nameAnnotation != null)
            commandName.setCommandName(nameAnnotation.name());

        return commandName;
    }
}
