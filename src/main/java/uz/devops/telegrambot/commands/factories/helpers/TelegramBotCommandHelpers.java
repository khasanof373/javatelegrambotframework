package uz.devops.telegrambot.commands.factories.helpers;

import org.reflections.Reflections;
import uz.devops.telegrambot.commands.TelegramBotCommand;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static org.reflections.scanners.Scanners.SubTypes;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.helpers
 * @since 1/19/2024 7:21 PM
 */
public class TelegramBotCommandHelpers {

    /**
     * @return
     */
    public static Set<Class<? extends TelegramBotCommand>> getCommandExtendedClasses() {
        try {
            return tryGetExtendedClasses();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return
     */
    private static Set<Class<? extends TelegramBotCommand>> tryGetExtendedClasses() {
        var packages = ClassLoader.getSystemClassLoader().getDefinedPackages();
        var cleanedPackages = Arrays.stream(packages).filter(f-> !isIgnoredPackage(f)).toList();
        var classesNames = cleanedPackages.stream().map(p -> getPackageCommandClasses(p.getName()));
        var classesNamesSet = classesNames.flatMap(Collection::stream).toList();

        return classesNamesSet
                .stream()
                .map(TelegramBotCommandHelpers::getClassByName)
                .collect(Collectors.toSet());
    }

    /**
     *
     * @param packageInfo
     * @return
     */
    private static boolean isIgnoredPackage(Package packageInfo) {
        var ignoredPackages = new ArrayList<String>();
        ignoredPackages.add("org.hibernate");
        ignoredPackages.add("org.apache");
        ignoredPackages.add("org.h2");
        ignoredPackages.add("org.telegram");
        ignoredPackages.add("org.slf4j");
        ignoredPackages.add("com.fasterxml");
        ignoredPackages.add("jakarta");
        ignoredPackages.add("net.bytebuddy");
        ignoredPackages.add("org.antlr");
        ignoredPackages.add("javax");
        ignoredPackages.add("eu.lestard");
        ignoredPackages.add("org.jboss");

        return ignoredPackages.stream().anyMatch(m-> packageInfo.getName().startsWith(m));
    }

    /**
     * @param name
     * @return
     */
    private static Class<? extends TelegramBotCommand> getClassByName(String name) {
        try {
            return Class.forName(name).asSubclass(TelegramBotCommand.class);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param packageName
     * @return
     */
    private static Set<String> getPackageCommandClasses(String packageName) {
        var reflections = new Reflections(packageName);
        return reflections.get(SubTypes.of(TelegramBotCommand.class));
    }

    /**
     * @param classType
     * @param annotationClass
     * @param <A>
     * @return
     */
    public static <A extends Annotation> A getAnnotationFromType(Class<?> classType, final Class<A> annotationClass) {
        while (!classType.getName().equals(Object.class.getName())) {
            if (classType.isAnnotationPresent(annotationClass)) {
                return classType.getAnnotation(annotationClass);
            }

            classType = classType.getSuperclass();
        }

        return null;
    }
}
