package uz.devops.telegrambot.commands.factories.models;

import lombok.Getter;
import lombok.Setter;
import uz.devops.telegrambot.commands.TelegramBotCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.models
 * @since 1/19/2024 6:56 PM
 */
public class TelegramBotCommandInfo {

    /**
     *
     */
    @Getter
    @Setter
    private long commandId;

    /**
     *
     */
    @Getter
    @Setter
    private TelegramBotCommandType commandType;

    /**
     *
     */
    @Getter
    @Setter
    private TelegramBotCommandName commandName;

    /**
     *
     */
    @Getter
    @Setter
    private boolean isHighCommand;

    /**
     *
     */
    @Getter
    @Setter
    private Class<? extends TelegramBotCommand> commandClass;

    /**
     *
     */
    @Getter
    @Setter
    private List<TelegramBotCommandInfo> nearCommands = new ArrayList<>();

    /**
     *
     * @param info
     */
    public void addNearCommand(TelegramBotCommandInfo info){
        nearCommands.add(info);
    }
}
