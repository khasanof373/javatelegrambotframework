package uz.devops.telegrambot.commands.factories.annotations;

import java.lang.annotation.*;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands
 * @since 1/22/2024 7:34 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = CommandNamesAnnotation.class)
@Target(ElementType.TYPE)
public @interface CommandNameAnnotation {

    /**
     *
     * @return
     */
    String lang() default "";

    /**
     *
     * @return
     */
    String name();
}
