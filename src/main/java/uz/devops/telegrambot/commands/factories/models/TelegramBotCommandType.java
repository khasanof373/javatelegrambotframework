package uz.devops.telegrambot.commands.factories.models;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.models
 * @since 1/19/2024 6:59 PM
 */
public enum TelegramBotCommandType {
    /**
     *
     */
    UNKNOWN,

    /**
     *
     */
    PUBLIC,

    /**
     *
     */
    INTERNAL,

    /**
     *
     */
    CALLBACK,

    /**
     *
     */
    INLINE,

    /**
     *
     */
    BACK
}