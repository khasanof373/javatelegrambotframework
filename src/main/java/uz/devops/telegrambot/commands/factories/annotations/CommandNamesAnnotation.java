package uz.devops.telegrambot.commands.factories.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.annotations
 * @since 2/5/2024 7:30 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CommandNamesAnnotation {
    /**
     *
     * @return
     */
    CommandNameAnnotation[] value();
}
