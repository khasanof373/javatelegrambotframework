package uz.devops.telegrambot.commands.factories.helpers;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.annotations.TelegramBotCommandAnnotation;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.helpers
 * @since 1/22/2024 7:39 PM
 */
public class TelegramBotCommandTypeHelper {

    /**
     * @param commandClass
     * @return
     */
    public static TelegramBotCommandType getType(Class<? extends TelegramBotCommand> commandClass) {
        var commandAnnotation = TelegramBotCommandHelpers.getAnnotationFromType(commandClass, TelegramBotCommandAnnotation.class);
        if (commandAnnotation == null)
            return TelegramBotCommandType.UNKNOWN;

        return commandAnnotation.commandType();
    }
}
