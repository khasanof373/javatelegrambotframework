package uz.devops.telegrambot.commands.factories;

import jakarta.activation.CommandInfo;
import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.annotations.*;
import uz.devops.telegrambot.commands.factories.helpers.TelegramBotCommandHelpers;
import uz.devops.telegrambot.commands.factories.helpers.TelegramBotCommandTypeHelper;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandInfo;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandType;
import uz.devops.telegrambot.database.services.commands.TelegramBotCommandEntityService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.managers.naming.TelegramBotNamingManager;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories
 * @since 1/19/2024 6:56 PM
 */
public class DefaultTelegramBotCommandFactory implements TelegramBotCommandFactory {

    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     *
     */
    private final TelegramBotCommandEntityService commandEntityService;

    /**
     *
     */
    private final TelegramBotNamingManager telegramBotNamingManager;

    /**
     *
     */
    private final List<TelegramBotCommandInfo> infos;

    /**
     *
     */
    public DefaultTelegramBotCommandFactory(ServiceProvider serviceProvider,
                                            TelegramBotCommandEntityService commandEntityService,
                                            TelegramBotNamingManager telegramBotNamingManager) {

        this.serviceProvider = serviceProvider;
        this.commandEntityService = commandEntityService;
        this.telegramBotNamingManager = telegramBotNamingManager;

        infos = new ArrayList<>();
        initCommands();
    }

    /**
     *
     */
    private void initCommands() {
        try {
            var commands = TelegramBotCommandHelpers.getCommandExtendedClasses();
            commands.forEach(this::initCommand);
            infos.forEach(this::initNearCommands);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param commandClass
     */
    private void initCommand(Class<? extends TelegramBotCommand> commandClass) {
        if (commandClass.isInterface() || Modifier.isAbstract(commandClass.getModifiers()))
            return;

        var commandType = TelegramBotCommandTypeHelper.getType(commandClass);
        var commandId = commandEntityService.getCommandId(commandClass);
        var isHighCommand = commandClass.isAnnotationPresent(TelegramBotHighPriorityCommandAnnotation.class);
        var info = new TelegramBotCommandInfo();

        info.setCommandId(commandId);
        info.setHighCommand(isHighCommand);
        info.setCommandClass(commandClass);
        info.setCommandType(commandType);

        telegramBotNamingManager.initCommandName(info);
        infos.add(info);
    }

    /**
     * @param info
     */
    private void initNearCommands(TelegramBotCommandInfo info) {
        var nearCommands = getNearCommands(info.getCommandClass());
        nearCommands.forEach(f -> info.addNearCommand(getCommandInfo(f)));
    }

    /**
     * @param commandClass
     * @return
     */
    private List<Class<? extends TelegramBotCommand>> getNearCommands(Class<? extends TelegramBotCommand> commandClass) {
        var nearCommands = commandClass.getDeclaredAnnotation(TelegramBotNearCommandsAnnotation.class);
        if (nearCommands != null)
            return Arrays.stream(nearCommands.value()).map(TelegramBotNearCommandAnnotation::commandType).collect(Collectors.toList());

        var nearCommand = commandClass.getAnnotation(TelegramBotNearCommandAnnotation.class);
        if (nearCommand != null)
            return List.of(nearCommand.commandType());

        return new ArrayList<>();
    }

    /**
     * @param command
     * @return
     */
    @Override
    public TelegramBotCommandInfo getCommandInfo(Class<? extends TelegramBotCommand> command) {
        return infos.stream().filter(f -> f.getCommandClass() == command).findFirst().get();
    }

    /**
     *
     * @param lang
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommandInfo getCommandInfo(String lang, String name) {
        return getCommandInfo(TelegramBotCommandType.PUBLIC, lang, name);
    }

    /**
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommandInfo getCommandInfo(String name) {
        return getCommandInfo(TelegramBotCommandType.PUBLIC, "", name);
    }

    /**
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommandInfo getCommandInfoByClassName(String name) {
        return infos.stream().filter(f -> f.getCommandClass().getSimpleName().equals(name)).findFirst().get();
    }


    /**
     * @param type
     * @param lang
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommandInfo getCommandInfo(TelegramBotCommandType type, String lang, String name) {
        return infos
                .stream()
                .filter(f -> f.getCommandType() == type &&
                        f.getCommandName().isEqualName(lang, name))
                .findFirst()
                .orElse(null);
    }

    /**
     * @param lang
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommand getBackCommand(String lang, String name) {
        var commandInfo = getCommandInfo(TelegramBotCommandType.BACK, lang, name);
        if (commandInfo == null)
            return null;

        return serviceProvider.getInstance(commandInfo.getCommandClass());
    }

    /**
     * @param lang
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommand getPublicCommand(String lang, String name) {
        var commandInfo = getCommandInfo(TelegramBotCommandType.PUBLIC, lang, name);
        if (commandInfo == null)
            return null;

        return serviceProvider.getInstance(commandInfo.getCommandClass());
    }

    /**
     * @param commandInfo
     * @return
     */
    @Override
    public TelegramBotCommand getCommand(TelegramBotCommandInfo commandInfo) {
        return serviceProvider.getInstance(commandInfo.getCommandClass());
    }

    /**
     * @param id
     * @return
     */
    @Override
    public TelegramBotCommand getCallbackCommandById(long id) {
        var commandInfo = infos.stream().filter(f ->
                f.getCommandId() == id &&
                        f.getCommandType() == TelegramBotCommandType.CALLBACK).findFirst();

        if (commandInfo.isEmpty())
            return null;

        return serviceProvider.getInstance(commandInfo.get().getCommandClass());
    }

    /**
     *
     */
    @Override
    public void init() {

    }
}
