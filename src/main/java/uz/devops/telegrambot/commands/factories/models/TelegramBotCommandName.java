package uz.devops.telegrambot.commands.factories.models;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.commands.factories.models
 * @since 1/19/2024 7:45 PM
 */
public class TelegramBotCommandName {

    /**
     *
     */
    @Getter
    @Setter
    private String commandName;

    /**
     *
     */
    private Map<String, String> commandNames = new HashMap<>();

    /**
     *
     * @param names
     */
    public void mergeNames(Map<String, String> names) {
        commandNames.putAll(names);
    }

    /**
     *
     * @param name
     * @return
     */
    public boolean isEqualName(String name){
        return isEqualName("", name);
    }

    /**
     *
     * @param name
     * @return
     */
    public boolean isEqualName(String lang, String name){
        if(commandName == null || commandName.isEmpty())
            return false;

        if(name == null || name.isEmpty())
            return false;

        if(lang == null || lang.isEmpty())
            return commandName.equals(name);

        if(commandNames.containsKey(lang))
            return commandNames.get(lang).equals(name);

        return commandName.equals(name);
    }
}
