package uz.devops.telegrambot.managers.caller;

import uz.devops.telegrambot.commands.TelegramBotCommand;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.managers.caller
 * @since 2/9/2024 7:40 PM
 */
public interface TelegramBotCommandCallManager {

    /**
     *
     * @param command
     */
    void call(TelegramBotCommand command);
}
