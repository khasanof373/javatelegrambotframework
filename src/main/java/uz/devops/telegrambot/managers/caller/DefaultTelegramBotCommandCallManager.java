package uz.devops.telegrambot.managers.caller;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.exceptions.TelegramBotExceptionHandler;
import uz.devops.telegrambot.managers.update.TelegramBotUpdateManager;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.managers.caller
 * @since 2/9/2024 7:40 PM
 */
public class DefaultTelegramBotCommandCallManager implements TelegramBotCommandCallManager {

    /**
     *
     */
    private ServiceProvider serviceProvider;

    /**
     *
     */
    private TelegramBotUpdateManager updateManager;

    /**
     * @param serviceProvider
     * @param updateManager
     */
    public DefaultTelegramBotCommandCallManager(ServiceProvider serviceProvider, TelegramBotUpdateManager updateManager) {
        this.serviceProvider = serviceProvider;
        this.updateManager = updateManager;
    }

    /**
     * 
     * @param command
     */
    @Override
    public void call(TelegramBotCommand command) {
        try {
            tryCallCommand(command);
        } catch (Exception exception) {
            catchCallCommand(exception, command);
        }
    }

    /**
     *
     * @param command
     */
    protected void tryCallCommand(TelegramBotCommand command) {
        command.initialize(serviceProvider);
        command.execute();
    }

    /**
     *
     * @param ex
     * @param command
     */
    protected void catchCallCommand(Exception ex, TelegramBotCommand command) {
        if(TelegramBotExceptionHandler.class.isAssignableFrom(command.getClass())) {
            ((TelegramBotExceptionHandler)command).exceptionHandle(updateManager.getCurrentUpdate(), ex);
            return;
        }

        var exceptionHandler = serviceProvider.getInstance(TelegramBotExceptionHandler.class);
        exceptionHandler.exceptionHandle(updateManager.getCurrentUpdate(), ex);
    }
}
