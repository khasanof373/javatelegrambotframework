package uz.devops.telegrambot.managers.naming;

import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;
import uz.devops.telegrambot.commands.factories.annotations.CommandNamesAnnotation;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandInfo;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandName;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.managers.naming
 * @since 2/5/2024 7:21 PM
 */
public class TelegramBotAnnotationNamingManager implements TelegramBotNamingManager {

    /**
     * @param commandInfo
     */
    @Override
    public void initCommandName(TelegramBotCommandInfo commandInfo) {
        var nameInfo = getCommandName(commandInfo);
        var nameAnnotations = getCommandNameAnnotations(commandInfo);

        if (nameAnnotations == null || nameAnnotations.length == 0)
            return;

        var defaultName = getDefaultNameAnnotation(nameAnnotations);
        if (defaultName.isPresent())
            nameInfo.setCommandName(defaultName.get().name());

        var nameMap = getNamesMap(nameAnnotations);
        nameInfo.mergeNames(nameMap);
    }

    /**
     * @param commandInfo
     * @return
     */
    private CommandNameAnnotation[] getCommandNameAnnotations(TelegramBotCommandInfo commandInfo) {
        var commandClass = commandInfo.getCommandClass();
        var namesAnnotation = commandClass.getDeclaredAnnotation(CommandNamesAnnotation.class);
        if (namesAnnotation != null)
            return namesAnnotation.value();

        var nameAnnotation = commandClass.getAnnotation(CommandNameAnnotation.class);
        if (nameAnnotation != null)
            return new CommandNameAnnotation[]{nameAnnotation};

        return new CommandNameAnnotation[0];
    }

    /**
     * @param commandInfo
     * @return
     */
    private TelegramBotCommandName getCommandName(TelegramBotCommandInfo commandInfo) {
        if (commandInfo.getCommandName() != null)
            return commandInfo.getCommandName();

        commandInfo.setCommandName(new TelegramBotCommandName());
        return commandInfo.getCommandName();
    }

    /**
     * @param names
     * @return
     */
    private Optional<CommandNameAnnotation> getDefaultNameAnnotation(CommandNameAnnotation[] names) {
        return Arrays.stream(names)
                .filter(f -> Objects.equals(f.lang(), ""))
                .findFirst();
    }

    /**
     * @param names
     * @return
     */
    private Map<String, String> getNamesMap(CommandNameAnnotation[] names) {
        return Arrays.stream(names)
                .map(s -> new AbstractMap.SimpleEntry<>(s.lang(), s.name()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
