package uz.devops.telegrambot.managers.naming;
import uz.devops.telegrambot.commands.factories.models.TelegramBotCommandInfo;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.managers.naming
 * @since 2/5/2024 7:21 PM
 */
public interface TelegramBotNamingManager {

    /**
     *
     * @param commandInfo
     * @return
     */
    void initCommandName(TelegramBotCommandInfo commandInfo);
}
