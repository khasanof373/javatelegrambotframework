package uz.devops.telegrambot.managers.update;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.managers.update
 * @since 1/31/2024 7:22 PM
 */
public interface TelegramBotUpdateManager {

    /**
     *
     * @param update
     */
    void setCurrentUpdate(Update update);

    /**
     *
     * @return
     */
    Update getCurrentUpdate();
}
