package uz.devops.telegrambot.managers.update;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.managers.update
 * @since 1/31/2024 7:22 PM
 */
public class DefaultTelegramBotUpdateManager implements TelegramBotUpdateManager {

    /**
     *
     */
    private Update currentUpdate;

    /**
     *
     * @param update
     */
    @Override
    public void setCurrentUpdate(Update update) {
        currentUpdate = update;
    }

    /**
     *
     * @return
     */
    @Override
    public Update getCurrentUpdate() {
        return currentUpdate;
    }

}
