package uz.devops.telegrambot.managers.session;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.database.services.users.TelegramBotUserService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.managers.update.TelegramBotUpdateManager;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot
 * @since 1/31/2024 7:19 PM
 */
public class DefaultTelegramBotSessionManager implements TelegramBotSessionManager {


    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     * @param serviceProvider
     */
    public DefaultTelegramBotSessionManager(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    /**
     * @param update
     */
    @Override
    public void startSession(Update update) {
        serviceProvider.startSession();

        var updateManager = serviceProvider.getInstance(TelegramBotUpdateManager.class);
        var userService = serviceProvider.getInstance(TelegramBotUserService.class);

        updateManager.setCurrentUpdate(update);
        userService.initUser(update);
    }

    /**
     * @param update
     */
    @Override
    public void endSession(Update update) {
        serviceProvider.endSession();
    }
}
