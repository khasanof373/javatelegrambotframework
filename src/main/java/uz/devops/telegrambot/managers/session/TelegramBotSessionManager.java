package uz.devops.telegrambot.managers.session;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot
 * @since 1/31/2024 7:19 PM
 */
public interface TelegramBotSessionManager {

    /**
     *
     * @param update
     */
    void startSession(Update update);

    /**
     *
     * @param update
     */
    void endSession(Update update);
}
