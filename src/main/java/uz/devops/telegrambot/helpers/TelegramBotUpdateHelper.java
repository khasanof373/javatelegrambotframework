package uz.devops.telegrambot.helpers;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import uz.devops.telegrambot.database.models.TelegramBotUserInfo;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.helpers
 * @since 1/31/2024 7:45 PM
 */
public class TelegramBotUpdateHelper {

    /**
     * @param update
     * @return
     */
    public static TelegramBotUserInfo convertUpdateToUserInfo(Update update) {
        var user = getUpdateUser(update);
        var userInfo = new TelegramBotUserInfo();

        userInfo.setUserName(user.getUserName());
        userInfo.setFirstName(user.getFirstName());
        userInfo.setSecondName(user.getLastName());
        userInfo.setTgChatId(getUpdateChatId(update));
        userInfo.setTgUserId(user.getId().toString());

        return userInfo;
    }

    /**
     *
     * @param update
     * @return
     */
    public static User getUpdateUser(Update update) {
        if(update.hasMessage())
            return update.getMessage().getFrom();

        if(update.hasCallbackQuery())
            return update.getCallbackQuery().getFrom();

        if(update.hasInlineQuery())
            return update.getInlineQuery().getFrom();

        return null;
    }

    /**
     *
     * @param update
     * @return
     */
    public static String getUpdateChatId(Update update) {
        if(update.hasMessage())
            return update.getMessage().getChatId().toString();

        if(update.hasCallbackQuery())
            return update.getCallbackQuery().getMessage().getChatId().toString();

        if(update.hasInlineQuery())
            return update.getInlineQuery().getFrom().getId().toString();

        return null;
    }
}
