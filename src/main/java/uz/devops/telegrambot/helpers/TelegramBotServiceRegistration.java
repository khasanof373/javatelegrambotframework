package uz.devops.telegrambot.helpers;

import uz.devops.telegrambot.TelegramBotManager;
import uz.devops.telegrambot.commands.factories.DefaultTelegramBotCommandFactory;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.database.configuration.*;
import uz.devops.telegrambot.database.repositories.commands.DefaultTelegramBotCommandRepository;
import uz.devops.telegrambot.database.repositories.commands.TelegramBotCommandRepository;
import uz.devops.telegrambot.database.repositories.users.DefaultTelegramBotUserRepository;
import uz.devops.telegrambot.database.repositories.users.TelegramBotUserRepository;
import uz.devops.telegrambot.database.services.commands.DefaultTelegramBotCommandEntityService;
import uz.devops.telegrambot.database.services.commands.TelegramBotCommandEntityService;
import uz.devops.telegrambot.database.services.users.DefaultTelegramBotUserService;
import uz.devops.telegrambot.database.services.users.TelegramBotUserService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.exceptions.DefaultTelegramBotExceptionHandler;
import uz.devops.telegrambot.exceptions.DefaultTelegramBotUnknownCommand;
import uz.devops.telegrambot.exceptions.TelegramBotExceptionHandler;
import uz.devops.telegrambot.exceptions.TelegramBotUnknownCommand;
import uz.devops.telegrambot.handlers.callback.DefaultTelegramBotCallbackQueryHandler;
import uz.devops.telegrambot.handlers.callback.TelegramBotInlineQueryHandler;
import uz.devops.telegrambot.handlers.inline.DefaultTelegramBotInlineQueryHandler;
import uz.devops.telegrambot.handlers.inline.TelegramBotCallbackQueryHandler;
import uz.devops.telegrambot.handlers.message.DefaultTelegramBotMessageHandler;
import uz.devops.telegrambot.handlers.message.TelegramBotMessageHandler;
import uz.devops.telegrambot.managers.caller.DefaultTelegramBotCommandCallManager;
import uz.devops.telegrambot.managers.caller.TelegramBotCommandCallManager;
import uz.devops.telegrambot.managers.naming.TelegramBotAnnotationNamingManager;
import uz.devops.telegrambot.managers.naming.TelegramBotNamingManager;
import uz.devops.telegrambot.managers.session.DefaultTelegramBotSessionManager;
import uz.devops.telegrambot.managers.session.TelegramBotSessionManager;
import uz.devops.telegrambot.managers.update.DefaultTelegramBotUpdateManager;
import uz.devops.telegrambot.managers.update.TelegramBotUpdateManager;
import uz.devops.telegrambot.messages.DefaultTelegramBotMessageManager;
import uz.devops.telegrambot.messages.TelegramBotMessageManager;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.helpers
 * @since 2/7/2024 9:19 PM
 */
public class TelegramBotServiceRegistration {

    /**
     *
     * @param serviceProvider
     * @param botManager
     */
    public static void RegisterServices(ServiceProvider serviceProvider, TelegramBotManager botManager) {
        serviceProvider.bindSingleton(ServiceProvider.class, serviceProvider);
        serviceProvider.bindSingleton(TelegramBotManager.class, botManager);
        serviceProvider.bindSingleton(TelegramBotNamingManager.class, TelegramBotAnnotationNamingManager.class);
        serviceProvider.bindSingleton(TelegramBotCommandFactory.class, DefaultTelegramBotCommandFactory.class);

        serviceProvider.bindInterface(TelegramBotMessageHandler.class, DefaultTelegramBotMessageHandler.class);
        serviceProvider.bindInterface(TelegramBotCallbackQueryHandler.class, DefaultTelegramBotCallbackQueryHandler.class);
        serviceProvider.bindInterface(TelegramBotInlineQueryHandler.class, DefaultTelegramBotInlineQueryHandler.class);
        serviceProvider.bindInterface(TelegramBotMessageManager.class, DefaultTelegramBotMessageManager.class);
        serviceProvider.bindInterface(TelegramBotExceptionHandler.class, DefaultTelegramBotExceptionHandler.class);
        serviceProvider.bindInterface(TelegramBotUnknownCommand.class, DefaultTelegramBotUnknownCommand.class);
        serviceProvider.bindInterface(TelegramBotSessionManager.class, DefaultTelegramBotSessionManager.class);
        serviceProvider.bindInterface(SessionFactoryService.class, DefaultSessionFactoryService.class);
        serviceProvider.bindInterface(DatabaseConfigurationFactory.class, DefaultDatabaseConfigurationFactory.class);
        serviceProvider.bindInterface(DatabaseConfig.class, DefaultDatabaseConfig.class);
        serviceProvider.bindInterface(TelegramBotCommandEntityService.class, DefaultTelegramBotCommandEntityService.class);
        serviceProvider.bindInterface(TelegramBotCommandRepository.class, DefaultTelegramBotCommandRepository.class);
        serviceProvider.bindInterface(TelegramBotCommandCallManager.class, DefaultTelegramBotCommandCallManager.class);

        serviceProvider.bindScopedInterface(TelegramBotUserService.class, DefaultTelegramBotUserService.class);
        serviceProvider.bindScopedInterface(TelegramBotUserRepository.class, DefaultTelegramBotUserRepository.class);
        serviceProvider.bindScopedInterface(TelegramBotUpdateManager.class, DefaultTelegramBotUpdateManager.class);
    }
}
