package uz.devops.telegrambot.helpers.keyboards.inline;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.devops.telegrambot.commands.bases.TelegramBotCallbackCommand;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.helpers.keyboards
 * @since 2/7/2024 8:34 PM
 */
public class InlineKeyboardBuilder {

    /**
     *
     */
    private final InlineKeyboardButton button;

    /**
     *
     */
    private final InlineKeyboardMarkupBuilder markupBuilder;

    /**
     *
     */
    private String text;

    /**
     *
     */
    private String data;

    /**
     *
     */
    private long commandId;

    /**
     *
     */
    private Class<? extends TelegramBotCallbackCommand> command;

    /**
     * @param button
     * @param markupBuilder
     */
    public InlineKeyboardBuilder(InlineKeyboardButton button, InlineKeyboardMarkupBuilder markupBuilder) {
        this.button = button;
        this.markupBuilder = markupBuilder;
    }

    /**
     * @param text
     * @return
     */
    public InlineKeyboardBuilder setText(String text) {
        this.text = text;
        this.button.setText(text);
        return this;
    }

    /**
     * @param data
     * @return
     */
    public InlineKeyboardBuilder setData(String data) {
        this.data = data;

        initButtonData();
        return this;
    }

    /**
     * @param command
     * @return
     */
    public InlineKeyboardBuilder setCommand(Class<? extends TelegramBotCallbackCommand> command) {
        this.command = command;
        var commandInfo = markupBuilder.getCommandFactory().getCommandInfo(command);
        if (commandInfo != null)
            commandId = commandInfo.getCommandId();

        initButtonData();
        return this;
    }

    /**
     *
     */
    private void initButtonData() {
        if (data == null || data.isEmpty())
            return;

        if (command == null)
            return;

        button.setCallbackData(commandId + "," + data);
    }
}
