package uz.devops.telegrambot.helpers.keyboards.reply;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.helpers.keyboards.reply
 * @since 2/9/2024 6:31 PM
 */
public class ReplyKeyboardMarkupBuilder {

    /**
     *
     */
    private KeyboardRow currentRow = new KeyboardRow();

    /**
     *
     */
    private List<KeyboardRow> rows = new ArrayList<>();

    /**
     *
     */
    public ReplyKeyboardMarkupBuilder() {
        rows.add(currentRow);
    }

    /**
     *
     * @param text
     * @return
     */
    public ReplyKeyboardMarkupBuilder addButton(String text) {
        currentRow.add(text);
        return this;
    }

    /**
     *
     * @return
     */
    public ReplyKeyboardMarkupBuilder newLine() {
        currentRow = new KeyboardRow();
        rows.add(currentRow);
        return this;
    }

    /**
     *
     * @return
     */
    public ReplyKeyboardMarkup build(){
        var keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setKeyboard(rows);

        return keyboardMarkup;
    }
}
