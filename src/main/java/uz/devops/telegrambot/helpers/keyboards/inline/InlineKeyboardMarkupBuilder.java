package uz.devops.telegrambot.helpers.keyboards.inline;

import lombok.Getter;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.helpers.keyboards
 * @since 2/5/2024 8:58 PM
 */
public class InlineKeyboardMarkupBuilder {

    /**
     *
     */
    private List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

    /**
     *
     */
    private List<InlineKeyboardButton> currentRow = new ArrayList<>();

    /**
     *
     */
    @Getter
    private TelegramBotCommandFactory commandFactory;

    /**
     *
     */
    public InlineKeyboardMarkupBuilder(TelegramBotCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
        keyboard.add(currentRow);
    }

    /**
     *
     * @return
     */
    public InlineKeyboardBuilder addButton(String text) {
        var button = new InlineKeyboardButton();
        button.setText(text);
        currentRow.add(button);

        return new InlineKeyboardBuilder(button, this);
    }

    /**
     *
     * @return
     */
    public InlineKeyboardMarkupBuilder AddNewRow(){
        currentRow = new ArrayList<>();
        keyboard.add(currentRow);

        return this;
    }

    /**
     *
     * @return
     */
    public InlineKeyboardMarkup build() {
        var markup = new InlineKeyboardMarkup();
        markup.setKeyboard(keyboard);

        return markup;
    }

}
