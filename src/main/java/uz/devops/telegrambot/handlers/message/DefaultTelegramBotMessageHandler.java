package uz.devops.telegrambot.handlers.message;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.messages.TelegramBotMessageManager;

import javax.inject.Singleton;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.handlers
 * @since 1/17/2024 7:18 PM
 */
@Singleton
public class DefaultTelegramBotMessageHandler implements TelegramBotMessageHandler {

    /**
     *
     */
    private TelegramBotMessageManager messageManager;

    /**
     *
     * @param messageManager
     */
    public DefaultTelegramBotMessageHandler(TelegramBotMessageManager messageManager) {
        this.messageManager = messageManager;
    }

    /**
     *
     * @param update
     */
    @Override
    public void handle(Update update) {
        messageManager.handle(update);
    }
}
