package uz.devops.telegrambot.handlers.message;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.handlers.message
 * @since 1/17/2024 7:41 PM
 */
public interface TelegramBotMessageHandler {

    /**
     *
     * @param update
     */
    void handle(Update update);
}
