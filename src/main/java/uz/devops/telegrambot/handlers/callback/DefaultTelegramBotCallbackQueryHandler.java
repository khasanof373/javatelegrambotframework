package uz.devops.telegrambot.handlers.callback;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.exceptions.TelegramBotExceptionHandler;
import uz.devops.telegrambot.handlers.inline.TelegramBotCallbackQueryHandler;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;

import javax.inject.Singleton;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.handlers
 * @since 1/17/2024 7:19 PM
 */
@Singleton
public class DefaultTelegramBotCallbackQueryHandler implements TelegramBotCallbackQueryHandler {

    /**
     *
     */
    private final TelegramBotCommandFactory commandFactory;

    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     * @param commandFactory
     * @param serviceProvider
     */
    public DefaultTelegramBotCallbackQueryHandler(TelegramBotCommandFactory commandFactory, ServiceProvider serviceProvider) {
        this.commandFactory = commandFactory;
        this.serviceProvider = serviceProvider;
    }

    /**
     * @param update
     */
    @Override
    public void handle(Update update) {
        var commandId = getCommandId(update);
        var command = commandFactory.getCallbackCommandById(commandId);
        if (command == null)
            return;

        executeCommand(update, command);
    }

    /**
     * @param update
     * @param command
     */
    private void executeCommand(Update update, TelegramBotCommand command) {
        try {
            tryExecuteCommand(command);
        } catch (Exception ex) {
            catchCallCommand(update, ex, command);
        }
    }

    /**
     * @param command
     */
    private void tryExecuteCommand(TelegramBotCommand command) {
        command.initialize(serviceProvider);
        command.execute();
    }

    /**
     * @param update
     * @param ex
     * @param command
     */
    protected void catchCallCommand(Update update, Exception ex, TelegramBotCommand command) {
        if (TelegramBotExceptionHandler.class.isAssignableFrom(command.getClass())) {
            ((TelegramBotExceptionHandler) command).exceptionHandle(update, ex);
            return;
        }

        var exceptionHandler = serviceProvider.getInstance(TelegramBotExceptionHandler.class);
        exceptionHandler.exceptionHandle(update, ex);
    }

    /**
     * @param update
     * @return
     */
    private long getCommandId(Update update) {
        var query = update.getCallbackQuery();
        var data = query.getData();
        if (data == null || data.isEmpty())
            return 0;

        if (!data.contains(",")) {
            return 0;
        }

        var idSplitterIndex = data.indexOf(",");
        var idText = data.substring(0, idSplitterIndex);

        return Long.parseLong(idText);
    }
}
