package uz.devops.telegrambot.handlers.callback;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.handlers.inline
 * @since 1/17/2024 7:42 PM
 */
public interface TelegramBotInlineQueryHandler {

    /**
     *
     * @param update
     */
    void handle(Update update);
}
