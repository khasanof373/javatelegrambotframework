package uz.devops.telegrambot.handlers.inline;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.exceptions.TelegramBotExceptionHandler;
import uz.devops.telegrambot.handlers.callback.TelegramBotInlineQueryHandler;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;

import javax.inject.Singleton;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.handlers
 * @since 1/17/2024 7:19 PM
 */
@Singleton
public class DefaultTelegramBotInlineQueryHandler implements TelegramBotInlineQueryHandler {

    /**
     *
     * @param update
     */
    public void handle(Update update) {

    }
}
