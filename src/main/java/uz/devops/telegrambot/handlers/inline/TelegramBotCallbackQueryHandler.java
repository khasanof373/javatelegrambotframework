package uz.devops.telegrambot.handlers.inline;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.handlers.inline
 * @since 1/17/2024 7:42 PM
 */
public interface TelegramBotCallbackQueryHandler {

    /**
     *
     *
     * @param update
     */
    void handle(Update update);
}
