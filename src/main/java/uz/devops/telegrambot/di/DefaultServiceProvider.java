package uz.devops.telegrambot.di;


import eu.lestard.easydi.EasyDI;
import lombok.SneakyThrows;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.di
 * @since 1/26/2024 6:56 PM
 */
public class DefaultServiceProvider implements ServiceProvider {

    /**
     *
     */
    private final ThreadLocal<Map<Class, Object>> mapThreadLocal;

    /**
     *
     */
    private final EasyDI easyDI;


    /**
     *
     */
    public DefaultServiceProvider() {
        mapThreadLocal = new InheritableThreadLocal<>();
        easyDI = new EasyDI();
    }

    /**
     * @param requestedType
     * @param <T>
     * @return
     */
    @Override
    public <T> T getInstance(Class<T> requestedType) {
        return easyDI.getInstance(requestedType);
    }

    /**
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    @Override
    public <T> void bindInterface(Class<T> interfaceType, Class<? extends T> implementationType) {
        easyDI.bindInterface(interfaceType, implementationType);
    }

    /**
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    @Override
    public <T> void bindScopedInterface(Class<T> interfaceType, Class<? extends T> implementationType) {
        easyDI.bindProvider(interfaceType, () -> getScopedInstance(interfaceType, implementationType));
    }

    /**
     * @param interfaceType
     * @param implementationType
     * @param <T>
     * @return
     */
    @SneakyThrows
    private <T> T getScopedInstance(Class<T> interfaceType, Class<? extends T> implementationType) {
        var map = mapThreadLocal.get();
        if (map == null) {
            map = new HashMap<>();
            mapThreadLocal.set(map);
        }

        if (map.containsKey(interfaceType)) {
            return (T) map.get(interfaceType);
        }

        var constructor = Arrays.stream(implementationType.getConstructors()).findFirst();
        var params = constructor.get().getParameterTypes();

        var paramObjs = Arrays.stream(params)
                .map(m -> easyDI.getInstance(m))
                .toArray();

        var instance = paramObjs.length == 0 ?
                constructor.get().newInstance() :
                constructor.get().newInstance(paramObjs);

        map.put(interfaceType, instance);
        return (T) instance;
    }

    /**
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    @Override
    public <T> void bindSingleton(Class<T> interfaceType, Class<? extends T> implementationType) {
        easyDI.bindProvider(interfaceType, () -> easyDI.getInstance(implementationType));
        easyDI.markAsSingleton(implementationType);
    }

    /**
     * @param interfaceType
     * @param instance
     * @param <T>
     */
    @Override
    public <T> void bindSingleton(Class<T> interfaceType, T instance) {
        easyDI.bindInstance(interfaceType, instance);

        if (interfaceType.isInterface())
            easyDI.markAsSingleton(instance.getClass());

        if (!interfaceType.isInterface())
            easyDI.markAsSingleton(interfaceType);
    }

    /**
     *
     */
    @Override
    public void startSession() {
        mapThreadLocal.set(new HashMap<>());
    }

    /**
     *
     */
    @Override
    public void endSession() {
        mapThreadLocal.remove();
    }

    /**
     *
     */
    @Override
    public void build() {

    }
}
