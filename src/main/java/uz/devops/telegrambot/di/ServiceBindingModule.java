package uz.devops.telegrambot.di;

import com.google.inject.AbstractModule;
import com.google.inject.binder.AnnotatedBindingBuilder;

import java.util.List;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.di
 * @since 1/26/2024 7:19 PM
 */
public class ServiceBindingModule extends AbstractModule {

    /**
     *
     */
    private List<Runnable> lazyBindMethods;

    /**
     *
     */
    public ServiceBindingModule(List<Runnable> lazyBindMethods) {
        this.lazyBindMethods = lazyBindMethods;
    }

    /**
     *
     */
    @Override
    protected void configure() {
        super.configure();
        lazyBindMethods.forEach(f->f.run());
    }

    /**
     *
     * @param key
     * @return
     * @param <T>
     */
    public <T> AnnotatedBindingBuilder<T> bind(Class<T> key) {
        return super.bind(key);
    }
}
