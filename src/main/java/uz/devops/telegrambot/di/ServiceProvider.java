package uz.devops.telegrambot.di;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.di
 * @since 1/26/2024 6:53 PM
 */
public interface ServiceProvider {

    /**
     *
     * @param requestedType
     * @return
     * @param <T>
     */
    <T> T getInstance(Class<T> requestedType);

    /**
     *
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    <T> void bindInterface(Class<T> interfaceType, Class<? extends T> implementationType);

    /**
     *
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    <T> void bindScopedInterface(Class<T> interfaceType, Class<? extends T> implementationType);

    /**
     *
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    <T> void bindSingleton(Class<T> interfaceType, Class<? extends T> implementationType);

    /**
     *
     * @param interfaceType
     * @param instance
     * @param <T>
     */
    <T> void bindSingleton(Class<T> interfaceType, T instance);

    /**
     *
     */
    void startSession();

    /**
     *
     */
    void endSession();

    /**
     *
     */
    void build();
}
