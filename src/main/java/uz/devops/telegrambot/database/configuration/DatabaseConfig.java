package uz.devops.telegrambot.database.configuration;

import lombok.*;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.configs
 * @since 2/5/2024 6:40 PM
 */
public interface DatabaseConfig {
    String getDriver = "org.h2.Driver";
    String getUrl = "jdbc:h2:file:./target/h2db/db/java_framework;DB_CLOSE_DELAY=-1";
    String getUsername = "java_framework";
    String getPassword = "2004";
    String getDialect = "org.hibernate.dialect.H2Dialect";
    String getShowSql = "true";
    String getDdlAuto = "update";
}
