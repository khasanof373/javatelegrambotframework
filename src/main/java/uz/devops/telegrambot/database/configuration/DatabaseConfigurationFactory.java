package uz.devops.telegrambot.database.configuration;

import org.hibernate.cfg.Configuration;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.configuration
 * @since 2/5/2024 6:42 PM
 */
public interface DatabaseConfigurationFactory {

    /**
     *
     * @return
     */
    Configuration create();
}
