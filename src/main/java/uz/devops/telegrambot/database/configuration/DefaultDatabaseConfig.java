package uz.devops.telegrambot.database.configuration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.configuration
 * @since 2/7/2024 7:04 PM
 */
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class DefaultDatabaseConfig implements DatabaseConfig {
    private final String driver = "org.h2.Driver";
    private final String url = "jdbc:h2:mem:java_framework";
    private final String username = "postgres";
    private final String password = "2004";
    private final String dialect = "org.hibernate.dialect.H2Dialect";
    private final String showSql = "true";
    private final String ddlAuto = "update";
}
