package uz.devops.telegrambot.database.configuration;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import uz.devops.telegrambot.database.models.TelegramBotCommandEntity;
import uz.devops.telegrambot.database.models.TelegramBotUserInfo;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.services
 * @since 2/5/2024 6:50 PM
 */
public class DefaultSessionFactoryService implements SessionFactoryService {

    /**
     *
     */
    private final Configuration configuration;

    /**
     *
     */
    private final DatabaseConfigurationFactory databaseConfigurationFactory;

    /**
     * @param databaseConfigurationFactory
     */
    public DefaultSessionFactoryService(DatabaseConfigurationFactory databaseConfigurationFactory) {
        this.configuration = databaseConfigurationFactory.create();
        this.databaseConfigurationFactory = databaseConfigurationFactory;
    }

    /**
     * @return
     */
    @Override
    public SessionFactory create() {
        configuration.addAnnotatedClass(TelegramBotUserInfo.class);
        configuration.addAnnotatedClass(TelegramBotCommandEntity.class);
        ServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();

        return configuration.buildSessionFactory(registry);
    }
}
