package uz.devops.telegrambot.database.configuration;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.configuration
 * @since 2/5/2024 6:44 PM
 */
public class DefaultDatabaseConfigurationFactory implements DatabaseConfigurationFactory {

    /**
     *
     */
    private final DatabaseConfig databaseConfig;

    /**
     *
     * @param databaseConfig
     */
    public DefaultDatabaseConfigurationFactory(DatabaseConfig databaseConfig) {
        this.databaseConfig = databaseConfig;
    }

    /**
     *
     * @return
     */
    @Override
    public Configuration create() {
        Configuration configuration = new Configuration();
        configuration.setProperties(properties());
        return configuration;
    }

    /**
     *
     * @return
     */
    private Properties properties() {
        Properties properties = new Properties();
        properties.setProperty(Environment.DRIVER, databaseConfig.getDriver);
        properties.setProperty(Environment.URL, databaseConfig.getUrl);
        properties.setProperty(Environment.USER, databaseConfig.getUsername);
        properties.setProperty(Environment.PASS, databaseConfig.getPassword);
        properties.setProperty(Environment.DIALECT, databaseConfig.getDialect);
        properties.setProperty(Environment.SHOW_SQL, databaseConfig.getShowSql);
        properties.setProperty(Environment.HBM2DDL_AUTO, databaseConfig.getDdlAuto);
        return properties;
    }
}
