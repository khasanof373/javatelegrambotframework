package uz.devops.telegrambot.database.configuration;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.session
 * @since 2/5/2024 6:49 PM
 */
public interface SessionFactoryService {

    SessionFactory create();

}
