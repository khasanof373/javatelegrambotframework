package uz.devops.telegrambot.database.services.users;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.commands.bases.TelegramBotBackCommand;
import uz.devops.telegrambot.commands.bases.TelegramBotInternalCommand;
import uz.devops.telegrambot.database.models.TelegramBotUserInfo;
import uz.devops.telegrambot.database.repositories.users.TelegramBotUserRepository;
import uz.devops.telegrambot.helpers.TelegramBotUpdateHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database
 * @since 1/26/2024 8:36 PM
 */
public class DefaultTelegramBotUserService implements TelegramBotUserService {

    /**
     *
     */
    private static Map<String ,TelegramBotUserInfo> userSessions = new HashMap<>();

    /**
     *
     */
    private final TelegramBotUserRepository userRepository;

    /**
     *
     */
    private TelegramBotUserInfo currentUser;

    /**
     * @param userRepository
     */
    public DefaultTelegramBotUserService(TelegramBotUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * @param update
     * @return
     */
    @Override
    public TelegramBotUserInfo initUser(Update update) {
        var userInfo = getUser(TelegramBotUpdateHelper.getUpdateChatId(update));
        if (userInfo == null) {
            userInfo = TelegramBotUpdateHelper.convertUpdateToUserInfo(update);
            userInfo = registerUser(userInfo);
        }

        setCurrentUser(userInfo);
        return userInfo;
    }

    /**
     * @param userInfo
     * @return
     */
    @Override
    public TelegramBotUserInfo registerUser(TelegramBotUserInfo userInfo) {
        userSessions.put(userInfo.getTgUserId(), userInfo);
        return userRepository.addUser(userInfo);
    }

    /**
     * @param chatId
     * @return
     */
    @Override
    public TelegramBotUserInfo getUser(String chatId) {
        if(userSessions.containsKey(chatId))
            return userSessions.get(chatId);

        return userRepository.getUser(chatId);
    }

    /**
     * @param userInfo
     */
    @Override
    public void setCurrentUser(TelegramBotUserInfo userInfo) {
        currentUser = userInfo;
    }


    /**
     *
     * @return
     */
    @Override
    public TelegramBotUserInfo setCurrentUserBackCommand(Class<? extends TelegramBotBackCommand> backCommand){
        currentUser.setBackCommand(backCommand.getSimpleName());
        return updateUserInfo(currentUser);
    }

    /**
     *
     * @param currentCommand
     * @return
     */
    @Override
    public TelegramBotUserInfo setCurrentUserCurrentCommand(Class<? extends TelegramBotInternalCommand> currentCommand){
        currentUser.setCurrentCommand(currentCommand.getSimpleName());
        return updateUserInfo(currentUser);
    }

    /**
     *
     * @return
     */
    @Override
    public TelegramBotUserInfo clearCurrentUserBackCommand(){
        currentUser.setBackCommand(null);
        return updateUserInfo(currentUser);
    }

    /**
     *
     * @return
     */
    @Override
    public TelegramBotUserInfo clearCurrentUserCurrentCommand(){
        currentUser.setCurrentCommand(null);
        return updateUserInfo(currentUser);
    }

    /**
     * @return
     */
    @Override
    public TelegramBotUserInfo getCurrentUser() {
        return currentUser;
    }

    /**
     *
     * @param language
     * @return
     */
    @Override
    public TelegramBotUserInfo setCurrentUserLanguage(String language) {
        if(currentUser == null)
            return null;

        currentUser.setLanguage(language);
        return updateUserInfo(currentUser);
    }

    /**
     *
     * @param userInfo
     * @return
     */
    @Override
    public TelegramBotUserInfo updateUserInfo(TelegramBotUserInfo userInfo) {
        return userRepository.updateUser(userInfo);
    }
}
