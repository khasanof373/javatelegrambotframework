package uz.devops.telegrambot.database.services.users;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.commands.bases.TelegramBotBackCommand;
import uz.devops.telegrambot.commands.bases.TelegramBotInternalCommand;
import uz.devops.telegrambot.database.models.TelegramBotUserInfo;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database
 * @since 1/26/2024 8:31 PM
 */
public interface TelegramBotUserService {

    /**
     *
     * @param update
     * @return
     */
    TelegramBotUserInfo initUser(Update update);

    /**
     *
     * @param userInfo
     * @return
     */
    TelegramBotUserInfo registerUser(TelegramBotUserInfo userInfo);

    /**
     *
     * @param chatId
     * @return
     */
    TelegramBotUserInfo getUser(String chatId);

    /**
     *
     * @return
     */
    void setCurrentUser(TelegramBotUserInfo userInfo);

    /**
     *
     * @return
     */
    TelegramBotUserInfo setCurrentUserBackCommand(Class<? extends TelegramBotBackCommand> backCommand);

    /**
     *
     * @param currentCommand
     * @return
     */
    TelegramBotUserInfo setCurrentUserCurrentCommand(Class<? extends TelegramBotInternalCommand> currentCommand);

    /**
     *
     * @return
     */
    TelegramBotUserInfo clearCurrentUserBackCommand();

    /**
     *
     * @return
     */
    TelegramBotUserInfo clearCurrentUserCurrentCommand();

    /**
     *
     * @return
     */
    TelegramBotUserInfo getCurrentUser();

    /**
     *
     * @param language
     * @return
     */
    TelegramBotUserInfo setCurrentUserLanguage(String language);

    /**
     *
     * @param userInfo
     * @return
     */
    TelegramBotUserInfo updateUserInfo(TelegramBotUserInfo userInfo);
}
