package uz.devops.telegrambot.database.services.commands;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.database.models.TelegramBotCommandEntity;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.services.commands
 * @since 2/7/2024 7:23 PM
 */
public interface TelegramBotCommandEntityService {

    /**
     *
     * @param command
     * @return
     */
    long getCommandId(Class<? extends TelegramBotCommand> command);

    /**
     *
     * @param name
     * @return
     */
    TelegramBotCommandEntity getCommandByName(String name);
}
