package uz.devops.telegrambot.database.services.commands;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.database.models.TelegramBotCommandEntity;
import uz.devops.telegrambot.database.repositories.commands.TelegramBotCommandRepository;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.services.commands
 * @since 2/7/2024 7:24 PM
 */
public class DefaultTelegramBotCommandEntityService implements TelegramBotCommandEntityService {

    /**
     *
     */
    private TelegramBotCommandRepository commandRepository;

    /**
     *
     * @param commandRepository
     */
    public DefaultTelegramBotCommandEntityService(TelegramBotCommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    /**
     *
     * @param command
     * @return
     */
    @Override
    public long getCommandId(Class<? extends TelegramBotCommand> command) {
        return commandRepository.migrate(command).getId();
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
    public TelegramBotCommandEntity getCommandByName(String name) {
        return commandRepository.getCommandByName(name);
    }
}
