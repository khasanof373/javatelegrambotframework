package uz.devops.telegrambot.database.repositories.commands;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.database.configuration.SessionFactoryService;
import uz.devops.telegrambot.database.models.TelegramBotCommandEntity;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.repositories.commands
 * @since 2/7/2024 7:15 PM
 */
public class DefaultTelegramBotCommandRepository implements TelegramBotCommandRepository {

    /**
     *
     */
    private final SessionFactory sessionFactory;

    /**
     * @param sessionFactoryService
     */
    public DefaultTelegramBotCommandRepository(SessionFactoryService sessionFactoryService) {
        this.sessionFactory = sessionFactoryService.create();
    }

    @Override
    public TelegramBotCommandEntity migrate(Class<? extends TelegramBotCommand> command) {
        var existsCommand = getCommandByName(command.getSimpleName());
        if (existsCommand != null)
            return existsCommand;

        var session = openSession();
        var transaction = session.beginTransaction();

        var commandEntity = new TelegramBotCommandEntity();
        commandEntity.setCommandName(command.getSimpleName());

        session.persist(commandEntity);
        transaction.commit();
        session.close();
        return commandEntity;
    }

    @Override
    public TelegramBotCommandEntity getCommandByName(String name) {
        var session = openSession();
        var query = session.createQuery("from TelegramBotCommandEntity where commandName=:commandName");
        query.setParameter(TelegramBotCommandEntity.Fields.commandName, name);

        var command = (TelegramBotCommandEntity) query.uniqueResult();
        session.close();
        return command;
    }

    /**
     * @return
     */
    private Session openSession() {
        return sessionFactory.openSession();
    }
}
