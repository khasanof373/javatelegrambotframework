package uz.devops.telegrambot.database.repositories.users;

import uz.devops.telegrambot.database.models.TelegramBotUserInfo;

import java.util.List;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.repositories
 * @since 1/31/2024 6:53 PM
 */
public interface TelegramBotUserRepository {

    /**
     *
     * @return
     */
    List<TelegramBotUserInfo> getUsers();

    /**
     *
     * @param id
     * @return
     */
    TelegramBotUserInfo getUser(long id);

    /**
     *
     * @param chatId
     * @return
     */
    TelegramBotUserInfo getUser(String chatId);

    /**
     *
     * @param userInfo
     * @return
     */
    TelegramBotUserInfo addUser(TelegramBotUserInfo userInfo);

    /**
     *
     * @param id
     * @return
     */
    TelegramBotUserInfo deleteUser(long id);

    /**
     *
     * @param chatId
     * @return
     */
    TelegramBotUserInfo deleteUser(String chatId);

    /**
     *
     * @param userInfo
     * @return
     */
    TelegramBotUserInfo updateUser(TelegramBotUserInfo userInfo);
}
