package uz.devops.telegrambot.database.repositories.commands;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.database.models.TelegramBotCommandEntity;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.repositories.commands
 * @since 2/7/2024 7:15 PM
 */
public interface TelegramBotCommandRepository {

    /**
     *
     * @param command
     * @return
     */
    TelegramBotCommandEntity migrate(Class<? extends TelegramBotCommand> command);

    /**
     *
     * @param name
     * @return
     */
    TelegramBotCommandEntity getCommandByName(String name);
}