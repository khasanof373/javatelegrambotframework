package uz.devops.telegrambot.database.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.models
 * @since 1/31/2024 6:55 PM
 */
@Data
@Entity
@FieldNameConstants
public class TelegramBotUserInfo {

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     *
     */
    private String userName;

    /**
     *
     */
    private String firstName;

    /**
     *
     */
    private String secondName;

    /**
     *
     */
    private String tgUserId;

    /**
     *
     */
    private String tgChatId;

    /**
     *
     */
    private String language;

    /**
     *
     */
    private String phoneNumber;

    /**
     *
     */
    private String currentCommand;

    /**
     *
     */
    private String currentCommandData;

    /**
     *
     */
    private String backCommand;
}
