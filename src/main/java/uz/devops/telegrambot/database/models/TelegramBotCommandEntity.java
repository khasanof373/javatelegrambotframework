package uz.devops.telegrambot.database.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.database.models
 * @since 2/7/2024 7:09 PM
 */
@Data
@Entity
@FieldNameConstants
public class TelegramBotCommandEntity {

    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     *
     */
    private String commandName;
}
