package uz.devops.telegrambot.exceptions;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.exceptions
 * @since 1/22/2024 8:34 PM
 */
public interface TelegramBotExceptionHandler {

    /**
     *
     * @param update
     * @param exception
     */
    void exceptionHandle(Update update, Exception exception);
}
