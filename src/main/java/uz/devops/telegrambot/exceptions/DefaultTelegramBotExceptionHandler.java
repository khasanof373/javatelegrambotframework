package uz.devops.telegrambot.exceptions;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.exceptions
 * @since 1/22/2024 8:55 PM
 */
public class DefaultTelegramBotExceptionHandler  implements TelegramBotExceptionHandler{

    /**
     *
     * @param update
     * @param exception
     */
    @Override
    public void exceptionHandle(Update update, Exception exception) {
        exception.printStackTrace();
    }
}
