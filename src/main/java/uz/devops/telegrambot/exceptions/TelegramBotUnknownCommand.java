package uz.devops.telegrambot.exceptions;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.exceptions
 * @since 1/22/2024 8:33 PM
 */
public interface TelegramBotUnknownCommand {

    /**
     *
     * @param update
     */
    void handle(Update update);
}
