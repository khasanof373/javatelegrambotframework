package uz.devops.telegrambot.exceptions;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.exceptions
 * @since 1/22/2024 8:57 PM
 */
public class DefaultTelegramBotUnknownCommand implements TelegramBotUnknownCommand {

    /**
     *
     * @param update
     */
    @Override
    public void handle(Update update) {

    }
}
