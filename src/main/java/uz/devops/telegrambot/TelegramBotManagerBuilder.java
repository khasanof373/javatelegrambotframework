package uz.devops.telegrambot;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.devops.telegrambot.configs.TelegramBotConfig;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot
 * @since 1/19/2024 6:32 PM
 */
public class TelegramBotManagerBuilder {

    /**
     *
     */
    private final TelegramBotConfig config;

    /**
     *
     */
    public TelegramBotManagerBuilder() {
        config = new TelegramBotConfig();
    }

    /**
     * @param token
     * @return
     */
    public TelegramBotManagerBuilder setToken(String token) {
        config.setToken(token);
        return this;
    }

    /**
     * @param userName
     * @return
     */
    public TelegramBotManagerBuilder setUserName(String userName) {
        config.setUserName(userName);
        return this;
    }


    /**
     *
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    public <T> TelegramBotManagerBuilder bindInterface(Class<T> interfaceType, Class<? extends T> implementationType) {
        config.bindInterface(interfaceType, implementationType);
        return this;
    }

    public TelegramBotManager start() throws TelegramApiException {
        return  TelegramBotManager.start(config);
    }
}
