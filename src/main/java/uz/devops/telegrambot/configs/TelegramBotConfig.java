package uz.devops.telegrambot.configs;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.configs
 * @since 1/17/2024 7:55 PM
 */
public class TelegramBotConfig {

    /**
     *
     */
    @Getter
    @Setter
    private String token;

    /**
     *
     */
    @Getter
    @Setter
    private String userName;

    /**
     *
     */
    @Getter
    Map<Class, Class> bindedInterfaces = new HashMap<>();

    /**
     *
     * @param interfaceType
     * @param implementationType
     * @param <T>
     */
    public <T> void bindInterface(Class<T> interfaceType, Class<? extends T> implementationType) {
        bindedInterfaces.put(interfaceType, implementationType);
    }
}
