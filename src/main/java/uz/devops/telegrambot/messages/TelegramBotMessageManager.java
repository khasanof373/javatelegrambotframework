package uz.devops.telegrambot.messages;

import org.telegram.telegrambots.meta.api.objects.Update;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers
 * @since 1/22/2024 6:40 PM
 */
public interface TelegramBotMessageManager {

    /**
     *
     */
    void handle(Update update);
}
