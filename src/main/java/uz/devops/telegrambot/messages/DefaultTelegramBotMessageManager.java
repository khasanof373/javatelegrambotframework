package uz.devops.telegrambot.messages;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;
import uz.devops.telegrambot.messages.handlers.CommandMessageHandler;
import uz.devops.telegrambot.messages.handlers.impls.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers
 * @since 1/22/2024 6:41 PM
 */
public class DefaultTelegramBotMessageManager implements TelegramBotMessageManager {

    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     *
     */
    private final List<Class<? extends CommandMessageHandler>> handlerTypes;

    /**
     *
     */
    public DefaultTelegramBotMessageManager(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;

        handlerTypes = new ArrayList<>();
        handlerTypes.add(HighPriorityCommandMessageHandler.class);
        handlerTypes.add(BackCommandMessageHandler.class);
        handlerTypes.add(NearCommandMessageHandler.class);
        handlerTypes.add(CurrentCommandMessageHandler.class);
        handlerTypes.add(PublicCommandMessageHandler.class);
        handlerTypes.add(UnknownCommandMessageHandler.class);
    }

    /**
     * @param update
     */
    @Override
    public void handle(Update update) {
        var handlerObjs = handlerTypes
                .stream()
                .map(m -> serviceProvider.getInstance(m))
                .collect(Collectors.toList());

        setNextHandlers(handlerObjs);
        var context = new CommandMessageContext();
        context.setUpdate(update);
        context.setServiceProvider(serviceProvider);

        var firstHandler = handlerObjs.get(0);
        firstHandler.handle(context);
    }

    /**
     *
     * @param handlers
     */
    private void setNextHandlers(List<? extends CommandMessageHandler> handlers) {
        for (var index = 0; index < handlers.size(); index++) {
            var handler = handlers.get(index);

            if(index <  handlers.size() - 1)
                handler.setNext(handlers.get(index + 1));
        }
    }
}
