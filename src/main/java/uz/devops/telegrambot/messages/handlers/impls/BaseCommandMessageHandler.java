package uz.devops.telegrambot.messages.handlers.impls;

import uz.devops.telegrambot.commands.TelegramBotCommand;
import uz.devops.telegrambot.exceptions.TelegramBotExceptionHandler;
import uz.devops.telegrambot.managers.caller.TelegramBotCommandCallManager;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;
import uz.devops.telegrambot.messages.handlers.CommandMessageHandler;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers.impls
 * @since 1/22/2024 6:55 PM
 */
public abstract class BaseCommandMessageHandler implements CommandMessageHandler {

    /**
     *
     */
    protected CommandMessageHandler nextHandler;

    /**
     * @param handler
     */
    @Override
    public void setNext(CommandMessageHandler handler) {
        nextHandler = handler;
    }

    /**
     * @param context
     */
    protected void callNextHandler(CommandMessageContext context) {
        if (nextHandler != null)
            nextHandler.handle(context);
    }

    /**
     * @param context
     * @param command
     */
    protected void callCommand(CommandMessageContext context, TelegramBotCommand command) {
       var callManager = context.getServiceProvider().getInstance(TelegramBotCommandCallManager.class);
       callManager.call(command);
    }
}
