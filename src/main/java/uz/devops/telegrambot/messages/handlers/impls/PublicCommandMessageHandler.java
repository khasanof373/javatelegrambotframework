package uz.devops.telegrambot.messages.handlers.impls;

import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.database.services.users.TelegramBotUserService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers.impls
 * @since 1/22/2024 6:55 PM
 */
public class PublicCommandMessageHandler extends BaseCommandMessageHandler {

    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     *
     */
    private final TelegramBotCommandFactory commandFactory;

    /**
     *
     */
    private final TelegramBotUserService userService;

    /**
     * @param serviceProvider
     * @param commandFactory
     * @param userService
     */
    public PublicCommandMessageHandler(ServiceProvider serviceProvider,
                                       TelegramBotCommandFactory commandFactory,
                                       TelegramBotUserService userService) {

        this.serviceProvider = serviceProvider;
        this.commandFactory = commandFactory;
        this.userService = userService;
    }

    /**
     * @param context
     */
    @Override
    public void handle(CommandMessageContext context) {
        var text = context.getUpdate().getMessage().getText();
        var lang = userService.getCurrentUser().getLanguage();

        var command = commandFactory.getPublicCommand(lang, text);
        if (command != null) {
            callCommand(context, command);
            return;
        }

        callNextHandler(context);
    }
}
