package uz.devops.telegrambot.messages.handlers.impls;

import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.database.services.users.TelegramBotUserService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers.impls
 * @since 1/22/2024 6:55 PM
 */
public class BackCommandMessageHandler extends BaseCommandMessageHandler {

    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     *
     */
    private final TelegramBotCommandFactory commandFactory;

    /**
     *
     */
    private final TelegramBotUserService userService;

    /**
     * @param serviceProvider
     * @param commandFactory
     * @param userService
     */
    public BackCommandMessageHandler(ServiceProvider serviceProvider,
                                     TelegramBotCommandFactory commandFactory,
                                     TelegramBotUserService userService) {

        this.serviceProvider = serviceProvider;
        this.commandFactory = commandFactory;
        this.userService = userService;
    }

    /**
     * @param context
     */
    @Override
    public void handle(CommandMessageContext context) {
        var userBackCommand = userService.getCurrentUser().getBackCommand();
        if(userBackCommand == null || userBackCommand.isEmpty())
        {
            callNextHandler(context);
            return;
        }

        var text = context.getUpdate().getMessage().getText();
        var lang = userService.getCurrentUser().getLanguage();

        var commandInfo = commandFactory.getCommandInfoByClassName(userBackCommand);
        if (commandInfo == null || !commandInfo.getCommandName().isEqualName(lang, text)) {
            callNextHandler(context);
            return;
        }

        var command = commandFactory.getCommand(commandInfo);
        callCommand(context, command);
    }
}
