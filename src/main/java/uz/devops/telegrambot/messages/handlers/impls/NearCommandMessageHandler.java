package uz.devops.telegrambot.messages.handlers.impls;

import uz.devops.telegrambot.commands.factories.TelegramBotCommandFactory;
import uz.devops.telegrambot.database.services.users.TelegramBotUserService;
import uz.devops.telegrambot.di.ServiceProvider;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers.impls
 * @since 2/14/2024 7:03 PM
 */
public class NearCommandMessageHandler extends BaseCommandMessageHandler {

    /**
     *
     */
    private final ServiceProvider serviceProvider;

    /**
     *
     */
    private final TelegramBotCommandFactory commandFactory;

    /**
     *
     */
    private final TelegramBotUserService userService;

    /**
     * @param serviceProvider
     * @param commandFactory
     * @param userService
     */
    public NearCommandMessageHandler(ServiceProvider serviceProvider, TelegramBotCommandFactory commandFactory, TelegramBotUserService userService) {
        this.serviceProvider = serviceProvider;
        this.commandFactory = commandFactory;
        this.userService = userService;
    }

    /**
     * @param context
     */
    @Override
    public void handle(CommandMessageContext context) {
        var userCurrentCommand = userService.getCurrentUser().getCurrentCommand();
        if (userCurrentCommand == null || userCurrentCommand.isEmpty()) {
            callNextHandler(context);
            return;
        }

        var commandInfo = commandFactory.getCommandInfoByClassName(userCurrentCommand);
        if (commandInfo == null) {
            callNextHandler(context);
            return;
        }

        var msg = context.getUpdate().getMessage();
        if (msg == null) {
            callNextHandler(context);
            return;
        }

        var currentLang = userService.getCurrentUser().getLanguage();
        var text = msg.getText();
        var nearCommand = commandInfo.getNearCommands()
                .stream()
                .filter(f -> f.getCommandName().isEqualName(currentLang, text))
                .findFirst();

        if (nearCommand.isEmpty()) {
            callNextHandler(context);
            return;
        }

        var command = commandFactory.getCommand(nearCommand.get());
        callCommand(context, command);
    }
}
