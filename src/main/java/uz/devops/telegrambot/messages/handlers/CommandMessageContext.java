package uz.devops.telegrambot.messages.handlers;

import lombok.Getter;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.di.ServiceProvider;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers
 * @since 1/22/2024 6:52 PM
 */
public class CommandMessageContext {

    /**
     *
     */
    @Getter
    @Setter
    Update update;

    /**
     *
     */
    @Getter
    @Setter
    ServiceProvider serviceProvider;
}
