package uz.devops.telegrambot.messages.handlers.impls;

import uz.devops.telegrambot.exceptions.TelegramBotUnknownCommand;
import uz.devops.telegrambot.messages.handlers.CommandMessageContext;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers.impls
 * @since 1/22/2024 6:57 PM
 */
public class UnknownCommandMessageHandler extends BaseCommandMessageHandler {

    /**
     *
     */
    private TelegramBotUnknownCommand unknownCommand;

    /**
     *
     * @param unknownCommand
     */
    public UnknownCommandMessageHandler(TelegramBotUnknownCommand unknownCommand) {
        this.unknownCommand = unknownCommand;
    }

    /**
     *
     * @param context
     */
    @Override
    public void handle(CommandMessageContext context) {
        unknownCommand.handle(context.getUpdate());
    }
}
