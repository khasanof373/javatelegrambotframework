package uz.devops.telegrambot.messages.handlers;

/**
 * @author Nurislom
 * @see uz.devops.telegrambot.messages.handlers
 * @since 1/22/2024 6:51 PM
 */
public interface CommandMessageHandler {

    /**
     *
     * @param handler
     */
    void setNext(CommandMessageHandler handler);

    /**
     *
     * @param context
     */
    void handle(CommandMessageContext context);
}
