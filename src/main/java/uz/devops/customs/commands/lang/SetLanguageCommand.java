package uz.devops.customs.commands.lang;

import uz.devops.telegrambot.commands.bases.TelegramBotCallbackCommand;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.lang
 * @since 2/5/2024 8:08 PM
 */
public class SetLanguageCommand extends TelegramBotCallbackCommand {

    /**
     *
     */
    @Override
    public void execute() {
        userService.setCurrentUserLanguage(commandData);
        sendAnswer("Til muvoffaqiyatli o'rnatildi");
    }
}
