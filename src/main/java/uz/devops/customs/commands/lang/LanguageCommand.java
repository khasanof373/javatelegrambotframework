package uz.devops.customs.commands.lang;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands
 * @since 2/5/2024 8:04 PM
 */
@CommandNameAnnotation(name = "/lang")
public class LanguageCommand extends TelegramBotPublicCommand {

    /**
     *
     */
    @Override
    public void execute() {
        var buttonsBuilder = createInlineButtonsBuilder();
        buttonsBuilder
                .addButton("ru")
                .setData("ru")
                .setCommand(SetLanguageCommand.class);

        buttonsBuilder.AddNewRow();
        buttonsBuilder
                .addButton("uz")
                .setData("uz")
                .setCommand(SetLanguageCommand.class);

        buttonsBuilder.AddNewRow();
        buttonsBuilder
                .addButton("en")
                .setData("en")
                .setCommand(SetLanguageCommand.class);

        var currentLang = userInfo.getLanguage();
        sendMessage("Tilni tanlang, Hozirgi til: " + currentLang, buttonsBuilder);
    }
}
