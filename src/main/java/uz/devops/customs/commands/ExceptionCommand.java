package uz.devops.customs.commands;

import org.telegram.telegrambots.meta.api.objects.Update;
import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;
import uz.devops.telegrambot.exceptions.TelegramBotExceptionHandler;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands
 * @since 1/22/2024 8:47 PM
 */
@CommandNameAnnotation(name = "/exception")
public class ExceptionCommand extends TelegramBotPublicCommand implements TelegramBotExceptionHandler  {

    /**
     *
     */
    @Override
    public void execute() {
        throw new RuntimeException("Test exception");
    }

    /**
     *
     * @param update
     * @param exception
     */
    public void exceptionHandle(Update update, Exception exception) {
        sendMessage("Hatolik yuz berdi!");
    }
}
