package uz.devops.customs.commands.menu;

import uz.devops.telegrambot.commands.bases.TelegramBotBackCommand;
import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.menu
 * @since 2/9/2024 6:49 PM
 */
@CommandNameAnnotation(name = "Ortga")
public class MainMenuBackCommand extends TelegramBotBackCommand {

    /**
     *
     */
    @Override
    public void execute() {
        sendMessage("Main menu back");
    }
}
