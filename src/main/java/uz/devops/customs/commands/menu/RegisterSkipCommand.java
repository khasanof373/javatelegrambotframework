package uz.devops.customs.commands.menu;

import uz.devops.telegrambot.commands.bases.TelegramBotInternalCommand;
import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.menu
 * @since 2/14/2024 7:30 PM
 */
@CommandNameAnnotation(name="Skip")
public class RegisterSkipCommand extends TelegramBotInternalCommand {

    /**
     *
     */
    @Override
    public void execute() {
        userService.clearCurrentUserCurrentCommand();
        callCommand(MainMenuCommand.class);
    }
}
