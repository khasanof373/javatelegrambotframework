package uz.devops.customs.commands.menu;

import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.menu
 * @since 2/9/2024 6:48 PM
 */
@CommandNameAnnotation(name = "/mainMenu")
public class MainMenuCommand extends TelegramBotPublicCommand {

    /**
     *
     */
    @Override
    public void execute() {
        var replyButtonsBuilder = createReplyButtonsBuilder();
        replyButtonsBuilder
                .addButton("/mainMenu")
                .addButton("/exception");

        replyButtonsBuilder.newLine();
        replyButtonsBuilder
                .addButton("/salom")
                .addButton("/privet")
                .addButton("/hello");

        replyButtonsBuilder.newLine();
        replyButtonsBuilder.addButton("Ortga");

        sendMessage("Bosh menu: Main menu", replyButtonsBuilder);
        userService.setCurrentUserBackCommand(MainMenuBackCommand.class);
    }
}