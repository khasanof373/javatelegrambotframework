package uz.devops.customs.commands.menu;

import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;

import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.menu
 * @since 2/9/2024 7:30 PM
 */
@CommandNameAnnotation(name = "/register")
public class RegisterCommand  extends TelegramBotPublicCommand {

    /**
     *
     */
    @Override
    public void execute() {
        sendMessage("Kutilgan malumotni yuboring!");
        userService.setCurrentUserBackCommand(MenuBackCommand.class);
        userService.setCurrentUserCurrentCommand(RegisterDataCommand.class);
    }
}
