package uz.devops.customs.commands.menu;

import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;
import uz.devops.telegrambot.commands.factories.annotations.TelegramBotCommandAnnotation;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.menu
 * @since 2/9/2024 6:40 PM
 */
@CommandNameAnnotation(name = "/menu")
public class MenuCommand extends TelegramBotPublicCommand {

    /**
     *
     */
    @Override
    public void execute() {
        var replyButtonsBuilder = createReplyButtonsBuilder();
        replyButtonsBuilder
                .addButton("/lang")
                .addButton("/exception");

        replyButtonsBuilder.newLine();
        replyButtonsBuilder
                .addButton("/salom")
                .addButton("/privet")
                .addButton("/hello");

        replyButtonsBuilder.newLine();
        replyButtonsBuilder.addButton("Ortga");

        sendMessage("Bosh menu: Menu", replyButtonsBuilder);
        userService.setCurrentUserBackCommand(MenuBackCommand.class);
    }
}
