package uz.devops.customs.commands.menu;

import uz.devops.telegrambot.commands.bases.TelegramBotInternalCommand;
import uz.devops.telegrambot.commands.factories.annotations.TelegramBotNearCommandAnnotation;

import java.util.Objects;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands.menu
 * @since 2/9/2024 7:34 PM
 */
@TelegramBotNearCommandAnnotation(commandType = RegisterSkipCommand.class)
public class RegisterDataCommand extends TelegramBotInternalCommand {

    /**
     *
     */
    @Override
    public void execute() {
        if(!Objects.equals(messageText, "123")) {
            sendMessage("To'g'ri malumot kirg'izing");
            return;
        }

        userService.clearCurrentUserCurrentCommand();
        callCommand(MenuCommand.class);
    }
}
