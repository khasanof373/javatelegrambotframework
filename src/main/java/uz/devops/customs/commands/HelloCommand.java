package uz.devops.customs.commands;

import uz.devops.telegrambot.commands.bases.TelegramBotPublicCommand;
import uz.devops.telegrambot.commands.factories.annotations.CommandNameAnnotation;
import uz.devops.telegrambot.commands.factories.annotations.TelegramBotHighPriorityCommandAnnotation;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands
 * @since 1/22/2024 8:16 PM
 */
@CommandNameAnnotation(name = "/salom")
@CommandNameAnnotation(lang = "ru",name = "/privet")
@CommandNameAnnotation(lang = "en", name = "/hello")
@TelegramBotHighPriorityCommandAnnotation
public class HelloCommand extends TelegramBotPublicCommand {

    /**
     *
     */
    @Override
    public void execute() {
        userService.clearCurrentUserCurrentCommand();
        userService.clearCurrentUserBackCommand();
        sendMessage("Hello world: " + messageText);
    }
}
