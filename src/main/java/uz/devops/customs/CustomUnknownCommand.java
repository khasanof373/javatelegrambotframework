package uz.devops.customs;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.devops.telegrambot.TelegramBotManager;
import uz.devops.telegrambot.exceptions.TelegramBotUnknownCommand;

/**
 * @author Nurislom
 * @see uz.devops.customs.commands
 * @since 1/22/2024 8:27 PM
 */
public class CustomUnknownCommand implements TelegramBotUnknownCommand {

    /**
     *
     */
    private TelegramBotManager botManager;

    /**
     *
     * @param botManager
     */
    public CustomUnknownCommand(TelegramBotManager botManager) {
        this.botManager = botManager;
    }

    /**
     *
     * @param update
     */
    @Override
    public void handle(Update update) {
        var sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText("Noaniq buyruq: " + update.getMessage().getText());

        try {
            botManager.execute(sendMessage);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }
}
